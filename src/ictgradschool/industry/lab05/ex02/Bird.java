package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    @Override
    public String sayHello() {
        return null;
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        return null;
    }

    @Override
    public int legCount() {
        return 0;
    }
}
